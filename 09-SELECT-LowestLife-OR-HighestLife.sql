SELECT * FROM country
WHERE LifeExpectancy = (SELECT MAX(LifeExpectancy) FROM country)
OR LifeExpectancy = (SELECT MIN(LifeExpectancy) FROM country);